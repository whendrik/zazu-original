apt update -y
apt install -y python3-pip
pip3 install -r /tete/requirements.txt
apt install -y locales
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
export LANG=en_US.UTF-8  
export LANGUAGE=en_US:en  
export LC_ALL=en_US.UTF-8  
export FLASK_APP=app.py
python3 -m flask run --host 0.0.0.0