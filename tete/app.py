from flask import Flask, render_template, request, send_from_directory, redirect, url_for
import os

app = Flask(__name__)
GAMES = "static/games/"

@app.route('/')
def index():
	files = os.listdir(GAMES)
	sgf_files = [file for file in files if file[-3:].lower() == "sgf" ]
	return render_template('board.html', files=sgf_files)
