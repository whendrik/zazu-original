var wgodiv = document.getElementById("wgoboard");
var player = {};

var game_root = {};

var position_counter = 0;
var child = null;

var my_username = 'kasuka';
var my_color = 0; // 1: BLACK, -1: WHITE
var array_of_mistakes = [];
var last_move_marks = [];

function DelayScanPositions(){
	setTimeout(ScanPositions, 500); 
}

function ScanPositions()
{
	position_counter = 0;
	array_of_mistakes = [];
	console.log(player);
	kifu = player.kifu;
	console.log(kifu.info.white.name);
	console.log(kifu.info.black.name);
	child = player.kifu.root;
	console.log(child);

	if(kifu.info.white.name == my_username){
		my_color = -1;
	}
	if(kifu.info.black.name == my_username){
		my_color = 1;
	}

	while( typeof child != "undefined"){
		position_counter += 1;
		//console.log(position_counter)
		if( child.comment.indexOf("mistake") > 0 && typeof child.move != "undefined"){
			//console.log(child.comment)
			top_5_mistakes = ["Top-1 mistake","Top-2 mistake","Top-3 mistake","Top-4 mistake","Top-5 mistake","Top-6 mistake"];
			var top_mistake = false;
			for( var i = 0; i < top_5_mistakes.length; i++){
				var mistake_string = top_5_mistakes[i];
				if( child.comment.indexOf(mistake_string) > 0 ){
					top_mistake = true;
				}
			}
			if( child.move.c == my_color && top_mistake){
				array_of_mistakes.push(position_counter);
			}
		}
		child = child.children[0];
	}

	
	console.log(array_of_mistakes);
	if( array_of_mistakes.length > 0 ){
		var random_position = array_of_mistakes[Math.floor(Math.random()*array_of_mistakes.length)];
		player.goTo(Math.max(0,random_position-2));
	}

}


function ResetMarkers(){
	player.board.removeObject(player.temp_marks);
	player.board.removeObject(last_move_marks);
	var last_move = player.kifuReader.node.move;
	var last_move_mark = {x:last_move.x,y:last_move.y,type:"CR"};
	last_move_marks.push( last_move_mark );
	player.board.addObject( last_move_mark );
}

function LoadWgo()
{
	last_move_marks = [];
	var random_file = sgf_files[Math.floor(Math.random()*sgf_files.length)];

	player = new WGo.BasicPlayer(wgodiv, {
	    sgfFile: "/static/games/" + random_file,
	    kifuLoaded: DelayScanPositions
	});
}

LoadWgo();