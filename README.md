# Auto-Analyze `.SGF` files with leela-zero

## Installation

Make sure to install `docker` and https://gitlab.com/whendrik/leelazerodocker

```
./docker.sh
...
cd /zazu
./init.sh
```

## Usage

usage: zazu.py [-h] [--playouts P] [--maxmove M] filename

For example
```
./zazu.py kasuka-Dienekes.sgf --playouts 1500
```
