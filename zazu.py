#!/usr/bin/python3

import sgf
import pexpect
import re
import argparse
import datetime


"""
Arguments
"""
parser = argparse.ArgumentParser()
parser.add_argument('filename',  help='SGF file to analyze')
parser.add_argument('--playouts', metavar='p', type=int, help='Number of playouts', default=750)
parser.add_argument('--maxmove', metavar='m', type=int, help='Till what move to Analyze', default=120)
args = parser.parse_args()

print(args)
SGF_FILE_TO_ANALYZE = args.filename
MAX_PLAYOUTS = args.playouts

PATH_TO_GTP_COMMAND = "/leela-zero/src/leelaz --weight /leela-zero/best-network -g --noponder -p {}".format(MAX_PLAYOUTS)


def gtp_to_sgf( coord ):
	sgf_system = "abcdefghijklmnopqrs"
	
	GOBAN_LETTERS = "ABCDEFGHJKLMNOPQRST"
	sgf_coords = re.compile("(?P<letter>[A-Z])(?P<number>\d+)")
	m = sgf_coords.match(coord)
	if m:
		letter = m.group("letter")
		number = int( m.group("number"))
		x_coord = GOBAN_LETTERS.find(letter)
		y_coord = 19 - number 
		sgf_coord = sgf_system[x_coord] + sgf_system[y_coord]
		return sgf_coord
	else:
		pass

def move_coord_to_gtp( move_coords ):
	sgf_coords, player = move_coords

	GOBAN_LETTERS = "ABCDEFGHJKLMNOPQRST"
	PLAYERS = [ "none", "black", "white"]
	if len(sgf_coords) > 1:
		x = ord( sgf_coords[0] ) - ord('a')
		y = ord( sgf_coords[1] ) - ord('a')
		GTP_COORDS = GOBAN_LETTERS[x] + str(19-y)
		return "play {} {}".format(player, GTP_COORDS)
	else:
		return None

def progressbar(percentage, prefix="Winrate Black: ", size=32):
	x = int( (percentage/100) * size)
	progress_text = "{} {:.2f}%\n|{}{}|\n".format(prefix, float(percentage), "#"*x, "_"*(size-x))
	return progress_text


moves_tuples = []
white_moves_played = 0
white_moves_leela = 0
black_moves_played = 0
black_moves_leela = 0

handicap_stones = 0

last_color_played = None

comments_for_sgf = {}

with open(SGF_FILE_TO_ANALYZE, encoding='utf-8') as f:
	collection = sgf.parse(f.read())
	gametree = collection.children[0]
	nodes = gametree.nodes
	for node in nodes:
		if "W" in node.properties:
			moves_tuples.append( (node.properties["W"][0], "white") )
			white_moves_played += 1
		if "B" in node.properties:
			moves_tuples.append( (node.properties["B"][0], "black") )
			black_moves_played += 1
		if "AB" in node.properties:
			for raw_move in node.properties["AB"]:
				moves_tuples.append( (raw_move, "black") )
				handicap_stones += 1


child = pexpect.spawn( PATH_TO_GTP_COMMAND, timeout=3600 )
child.expect(".*")
child.sendline( "time_settings 0 3600 1" )
child.expect("=.*")

def parse_leela_zero_log( line ):
	"Playouts: 70, Win: 48.48%, PV: D4 C3 C4 D3 F3 E3 E4 F2 Q4"
	log_re = re.compile("Playouts: (?P<playouts>\d+), Win: +(?P<win>\d+.\d+)%, PV: (?P<sequence>.*)")
	m = log_re.match(line)
	if m:
		return ( int(m.group("playouts")) , float(m.group("win")), m.group('sequence')) 
	else:
		return ()

def parse_leela_zero_log_variations( line ):
	"  Q4 ->       6 (V: 52.78%) (N: 44.84%) PV: Q4 C3 E3 C5 C6"
	log_re = re.compile(".* -> +(?P<playouts>\d+) +\(V: +(?P<win>\d+.\d+)%\) +.*PV: (?P<variation>.*)")
	m = log_re.match(line)
	if m:
		return ( int(m.group("playouts")) , float(m.group("win")), m.group('variation')) 
	else:
		return ()

def check_resign(line):
	"Eval (6.29%) looks bad. Resigning."
	resign_re = re.compile(".* Resigning.*")
	m = resign_re.match(line)
	if m:
		return True
	else:
		return False

game_winrates = []
white_winrates = []
black_winrates = []

move_number = 0

leela_sequence = {}
leela_alternatives = {}

variations = {}
mistakes_comments = {}

best_guess_winrate = None

for move in moves_tuples:
	move_number += 1

	if args.maxmove:
		if args.maxmove + handicap_stones < move_number:
			print( "REACHING MAX MOVE ({}) TO ANALYZE".format(args.maxmove))
			break

	move_text = move_coord_to_gtp(move)
	last_color_played = move[1]
	next_color = "black"
	if last_color_played == "black":
		next_color = "white"
	print("move number: {}".format(move_number))
	print(move_text)
	if best_guess_winrate:
		print( progressbar(best_guess_winrate[1]) )
		comments_for_sgf[move_number-1] = progressbar(best_guess_winrate[1])

	if move_text:
		child.sendline( move_text )
		child.expect("= .*")

	genmove_text = "genmove {}".format( next_color )
	child.sendline( "showboard" )
	child.expect("= .*")
	child.sendline( genmove_text )
	child.expect("= .*")
	GTP_OUTPUT = child.before
	GTP_STRING = GTP_OUTPUT.decode("utf-8")
	print(GTP_STRING)
	CHUNKS = GTP_STRING.split("\n")

	current_winrates = []
	current_winrates_variations = []

	resign = False

	for chunk in CHUNKS:
		if check_resign(chunk):
			resign = True

		parse_try = parse_leela_zero_log(chunk)
		if parse_try:
			playouts, winrate, sequence = parse_try
			if last_color_played == "white":
				current_winrates.append( (playouts,winrate,move_number,last_color_played, sequence) )
			else:
				current_winrates.append( (playouts,100-winrate,move_number,last_color_played, sequence) )

		
		estimate_try = parse_leela_zero_log_variations(chunk)
		if estimate_try:
			playouts, winrate, sequence = estimate_try
			if last_color_played == "white":
				current_winrates_variations.append( (playouts,winrate,move_number,last_color_played, sequence) )
			else:
				current_winrates_variations.append( (playouts,100-winrate,move_number,last_color_played, sequence) )

	best_guess_winrate = current_winrates_variations[0]
	if current_winrates:
		best_guess_winrate = current_winrates[-1]
	
	leela_sequence[move_number+1] = best_guess_winrate[4]

	game_winrates.append( best_guess_winrate )

	variations[move_number+1] = current_winrates_variations
	winrate_diff = [ (c[1]-p[1],c[2],c[3],c[1],p[4]) for (c,p) in zip(game_winrates[1:],game_winrates[:-1])]

	MISTAKE_THRESHOLD = 2
	top_mistake_comments = ""

	for color in ["black", "white"]:
		print('---')
		print( "## {}'s mistakes:".format(color))
		print('---')
		sign = -1
		if color == "black":
			sign = 1

		ordered_by_impact = sorted(winrate_diff, key=lambda x: sign*x[0])
		
		mistakes = [ m for m in ordered_by_impact if m[2] == color and -1 * sign * m[0] > MISTAKE_THRESHOLD]
		impact_count = 0
		top_mistake_comments += "\n\n$$$ TOP MISTAKE FOR {}\n".format(color)
		for m in mistakes:
			impact_count += 1
			print( "move {}, lost {}% winrate, for {} winrateblack: {}".format(m[1] - handicap_stones,m[0],m[2],m[3]) )
			move = m[1]
			played = move_coord_to_gtp(moves_tuples[move-1])
			print(played)
			print("alternatives:")
			for alternative in variations[move]:
				first_move = alternative[4].split(" ")[0]
				sequence_without_cr = alternative[4].replace("\r","")
				print( " {} -> {} (for {}) - {}".format( first_move,alternative[1],m[2],sequence_without_cr ) )

			print("----------")
			mistakes_comments[ move ] = "\n--- Top-{} mistake (most impact) ---\n".format(impact_count)
			mistakes_comments[ move ] += "move {}, lost {}% winrate. winrate: {}".format(m[1] - handicap_stones,m[0],m[3])
			mistakes_comments[ move ] += "\n " + str(played)
			mistakes_comments[ move ] += "\n " + "Alternatives: "
			for alternative in variations[move]:
				mistakes_comments[ move ] += "\n " + " {} -> {} (for {})".format( alternative[4],alternative[1],m[2] )

			if impact_count <= 5:
				top_mistake_comments += "\n--- Top-{} mistake (most impact) ---\n".format(impact_count)
				top_mistake_comments += "move {}, lost {}% winrate. winrate: {}".format(m[1] - handicap_stones,m[0],m[3])
				top_mistake_comments += "\n " + str(played)
				top_mistake_comments += "\n " + "Alternatives: "
				for alternative in variations[move]:
					top_mistake_comments += "\n " + " {} -> {} (for {})".format( alternative[4],alternative[1],m[2] )


	if not resign:
		child.sendline( "undo" )
		child.expect("= .*")



"""
	Add comments to SGF
"""

TOP_N_MISTAKES = 5

collection = None
print("handicap_stones")
print(handicap_stones)

print("-- writing output to sgf --")
with open(SGF_FILE_TO_ANALYZE, encoding='utf-8') as f:
	collection = sgf.parse(f.read())
	gametree = collection.children[0]
	nodes = gametree.nodes
	for i, node in enumerate(nodes):
		move_count = i + handicap_stones
		if i == 0:
			"""FIRST"""
			node.properties["C"] = ["ZaZu v0.7 - Automatic SGF review with Leela Zero\n"]
			node.properties["C"][0] += "{}".format(top_mistake_comments ).replace("[","\[").replace("]","\]")
			
		if "W" in node.properties:
			moves_tuples.append( (node.properties["W"][0], "white") )
			white_moves_played += 1
		if "B" in node.properties:
			moves_tuples.append( (node.properties["B"][0], "black") )
			black_moves_played += 1
		if "AB" in node.properties:
			for raw_move in node.properties["AB"]:
				moves_tuples.append( (raw_move, "black") )
		if i > 0:
			node.properties["C"] = [""]
		if move_count in comments_for_sgf:
			#node.properties["C"][0] += "{}".format( comments_for_sgf[move_count] ).replace("[","\[").replace("]","\]")
			node.properties["C"][0] += "{}".format( comments_for_sgf[move_count] )
		
		if move_count in leela_sequence:
				node.properties["C"][0] += "\n\n Leela's Suggestions: " + leela_sequence[move_count] + "\n"
				node.properties["C"][0] += "\n\n Alternatives: \n"
		bogus = node.properties.pop('LB', None)
		if move_count in variations:
				markers = "ABCDEFGHJKLMNOPQRST"
				marker_count = 0
				for var in variations[move_count]:
						first_move = var[4].split(" ")[0]
						sequence_without_cr = var[4].replace("\r","")
						var_string = " {} -> {} - {}".format( first_move,var[1],sequence_without_cr )
						node.properties["C"][0] += var_string + "\n"
						HUMAN_COORD = first_move.strip()
						if "LB" in node.properties:
							marker_count += 1
							if marker_count < len(markers):
								marker = markers[marker_count]
								node.properties["LB"] += [ "{}:{}".format(gtp_to_sgf(HUMAN_COORD), marker) ]
						else:
							node.properties["LB"] = [ "{}:A".format(gtp_to_sgf(HUMAN_COORD)) ]
		
		if move_count in mistakes_comments:
			node.properties["C"][0] += "\n\n " + mistakes_comments[move_count]
			
UNIX_SECONDS = datetime.datetime.strftime(datetime.datetime.now(),"%s")

OUTPUT_FILENAME = "{}_zazu-{}.sgf".format( SGF_FILE_TO_ANALYZE[:-3], str(UNIX_SECONDS))

with open(OUTPUT_FILENAME, "w") as f:
	collection.output(f)
print( "Written output to file {}".format(OUTPUT_FILENAME))